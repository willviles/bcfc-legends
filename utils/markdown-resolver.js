const PersistentFilter = require('broccoli-persistent-filter')
const MergeTrees = require('broccoli-merge-trees')
const Funnel = require('broccoli-funnel')
const frontmatter = require('front-matter')

function mergeTrees (trees, options) {
  let mergedOptions = Object.assign({ overwrite: true }, options)
  return trees.length === 1 ? trees[0] : new MergeTrees(trees, mergedOptions)
}

module.exports = function (app, options = {}) {
  let publicMdFiles = new Funnel('src', {
    annotation: `Move mdFiles to public`,
    include: ['**/*.md'],
    getDestinationPath (relativePath) {
      return relativePath
    }
  })

  let publicMdJson = new MarkdownJSONifier('src', {
    annotation: `Generate .json representations of files`
  })

  let publicMdJs = new MarkdownJSifier(publicMdJson, {
    annotation: `Place resolvable .js representations of markdown json files in src`
  })

  if (options.collections) {
    for (const [name, config] of Object.entries(options.collections)) {
      // console.log(name, config)
      // if (!config.inputPath) continue
    }
  }

  app.trees.public = mergeTrees([app.trees.public, publicMdFiles, publicMdJson, publicMdJs])
}

MarkdownJSONifier.prototype = Object.create(PersistentFilter.prototype)
MarkdownJSONifier.prototype.constructor = MarkdownJSONifier

function MarkdownJSONifier (inputNode, options) {
  options = options || {}
  PersistentFilter.call(this, inputNode, {
    annotation: options.annotation
  })
}

MarkdownJSONifier.prototype.extensions = ['md']
MarkdownJSONifier.prototype.targetExtension = 'md.json'

MarkdownJSONifier.prototype.processString = function (raw, relativePath) {
  const { body, attributes } = frontmatter(raw)
  return JSON.stringify({
    path: relativePath,
    frontmatter: attributes,
    content: escape(body)
  })
}

MarkdownJSifier.prototype = Object.create(PersistentFilter.prototype)
MarkdownJSifier.prototype.constructor = MarkdownJSifier

function MarkdownJSifier (inputNode, options) {
  options = options || {}
  PersistentFilter.call(this, inputNode, {
    annotation: options.annotation
  })
}

MarkdownJSifier.prototype.extensions = ['md.json']
MarkdownJSifier.prototype.targetExtension = 'md.js'

MarkdownJSifier.prototype.processString = function (raw, relativePath) {
  const { path, frontmatter, content } = JSON.parse(raw)
  return `
export const path = \`${path}\`
export const frontmatter = ${JSON.stringify(frontmatter)}
export const content = \`${content}\`
export default {
  path, frontmatter, content
}
`
}
