import Serializer from 'ember-data/serializer'

export default class ApplicationSerializer extends Serializer {
  primaryKey = 'id'

  normalizeResponse (store, primaryModelClass, payload, id, requestType) {
    if (requestType === 'findRecord') {
      return this.normalize(primaryModelClass, payload)
    } else {
      return this.normalizeArrayResponse(...arguments)
    }
  }

  normalizeArrayResponse (store, primaryModelClass, payload, id, requestType) {
    return payload.reduce((documentHash, item) => {
      let { data } = this.normalize(primaryModelClass, item)
      documentHash.data.push(data)
      return documentHash
    }, { data: [] })
  }

  normalize (modelClass, resourceHash) {
    const data = {
      id: resourceHash[this.primaryKey || 'id'],
      type: modelClass.modelName,
      attributes: resourceHash
    }
    return { data }
  }
}
