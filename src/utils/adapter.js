import Adapter from 'ember-data/adapter'
import ENV from '../../config/environment'

export default class ApplicationAdapter extends Adapter {
  baseURL = `${ENV.API.host}${ENV.API.namespace}`
}
