import Adapter from 'ember-data/adapter'
import { dasherize } from '@ember/string'
import { reject } from 'rsvp'
import moment from 'moment'

const EVENTS = [{
  title: 'Geoff Horsfield Foundation Golf Day',
  excerpt: `A golf day at a prestigious course in aid of Geoff Horsfield's charity.`,
  publishedAt: moment().subtract(10, 'd'),
  startAt: moment().add(10, 'd'),
  location: `The Belfry Golf Course`
}, {
  title: 'Villa All Stars (A)',
  publishedAt: moment().subtract(14, 'd'),
  startAt: moment().add(20, 'd'),
  location: `St. Andrew's Stadium`,
  tags: ['all-stars-fixture']
}, {
  title: 'Leicester All Stars (H)',
  publishedAt: moment().subtract(34, 'd'),
  startAt: moment().subtract(6, 'd'),
  location: `St. Andrew's Stadium`,
  tags: ['all-stars-fixture']
}, {
  title: 'An Evening with Tom Ross',
  publishedAt: moment().subtract(16, 'd'),
  startAt: moment().add(25, 'd'),
  location: `St. Andrew's Stadium`
}, {
  title: 'Wolves All Stars (H)',
  publishedAt: moment().subtract(33, 'd'),
  startAt: moment().subtract(2, 'd'),
  location: `St. Andrew's Stadium`,
  tags: ['all-stars-fixture']
}].map(e => {
  e.slug = dasherize(e.title)
  e.publishedAt = e.publishedAt.format(`YYYY-MM-DDTHH:mm:ss.SSSZ`)
  e.startAt = e.startAt.format(`YYYY-MM-DDTHH:mm:ss.SSSZ`)
  e.content = `
  <p>Maecenas quis interdum erat. Aenean augue libero, lacinia sit amet neque vel, finibus viverra nibh. Duis at turpis sed elit luctus dignissim a vel dolor. Praesent justo ex, semper sit amet augue ornare, rutrum rutrum mi. Sed laoreet est ante, id dapibus mi luctus et. Aenean venenatis nulla mi, a finibus odio laoreet vitae. Nulla ut dolor consectetur, feugiat erat nec, efficitur turpis. Curabitur consequat laoreet tortor, at varius eros tempor non. Integer neque sem, interdum vel ex quis, lacinia scelerisque mauris. In laoreet justo ante, quis tincidunt felis dapibus vel. Nullam vel ante urna. Duis velit neque, eleifend ut dapibus congue, pharetra at quam. Aenean enim eros, convallis eu lectus vitae, fermentum placerat risus. Proin suscipit ligula libero, et vestibulum massa tempus pharetra. Maecenas sed posuere nisi.</p>
  <p>Morbi elementum, nunc id sollicitudin euismod, sapien dolor bibendum nisi, at iaculis lorem odio non neque. Cras tristique magna quis mollis hendrerit. Curabitur laoreet, purus aliquam consectetur gravida, turpis est sollicitudin dui, non sollicitudin eros risus eget nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris lacinia, velit sagittis varius fermentum, massa urna dapibus ligula, nec ultrices augue ligula sit amet ante. Nunc vestibulum sollicitudin dictum. Quisque accumsan nisi leo, et facilisis dui blandit sed. Donec sed arcu non tortor rutrum euismod. Quisque at bibendum nisi. Fusce lorem risus, congue ut bibendum id, consectetur et elit. Maecenas mauris dui, ullamcorper quis aliquam eu, pulvinar in nunc.</p>
  `
  return e
})

export default class EventAdapter extends Adapter {
  findRecord (store, type, id, snapshot) {
    let article = EVENTS.find(article => article.slug === id)
    if (!article) reject({ id })
    return article
  }

  query (store, type, query) {
    return EVENTS.filter(event => {
      if (query.timeframe === 'upcoming' && moment().unix() > moment(event.startAt).unix()) return false
      if (query.timeframe === 'past' && moment().unix() < moment(event.startAt).unix()) return false
      if (query.tags && (!event.tags || !query.tags.some(tag => event.tags.includes(tag)))) return false
      return true
    })
  }
}
