import ApplicationSerializer from '../../../utils/serializer'

export default class EventSerializer extends ApplicationSerializer {
  primaryKey = 'slug'

  normalize () {
    let { data } = super.normalize(...arguments)
    if (data.attributes.frontmatter) {
      data.attributes.title = data.attributes.frontmatter.title
    }
    return { data }
  }
}
