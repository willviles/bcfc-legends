import Model from 'ember-data/model'
import { attr } from '@ember-decorators/data'

export default class EventModel extends Model {
  @attr('string') slug
  @attr('string') title
  @attr('string') content
  @attr('string') excerpt
  @attr('date') publishedAt
  @attr('date') startAt
  @attr('date') endAt
  @attr('string') location
}
