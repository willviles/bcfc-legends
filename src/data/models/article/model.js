import Model from 'ember-data/model'
import { attr } from '@ember-decorators/data'

export default class ArticleModel extends Model {
  @attr('string') slug
  @attr('string') title
  @attr('string') excerpt
  @attr('string') content
  @attr('date') publishedAt
}
