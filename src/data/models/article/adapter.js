import ApplicationAdapter from '../../../utils/adapter'
import { dasherize } from '@ember/string'
import { reject } from 'rsvp'
import moment from 'moment'
// import fetch from 'fetch'

const ARTICLES = [{
  title: 'Geoff Horsfield Foundation wins awards',
  excerpt: `A catch up with Barnsley-born striker Geoff Horsfield to look back on his career, which included a memorable three years at St. Andrew's.`,
  publishedAt: moment().subtract(1, 'd')
}, {
  title: `Mikael Forssell joins the Former Players' Association`,
  excerpt: `Former Finnish hotshot Mikael Forssell has finally joined the ranks of Blues Legends.`,
  publishedAt: moment().subtract(10, 'd')
}, {
  title: `Trevor Francis supports local charity`,
  excerpt: `Money pours into local charity after Trevor Francis book signing.`,
  publishedAt: moment().subtract(14, 'd')
}].map(a => {
  a.slug = dasherize(a.title)
  a.publishedAt = a.publishedAt.format(`YYYY-MM-DDTHH:mm:ss.SSSZ`)
  a.content = `<p>Maecenas quis interdum erat. Aenean augue libero, lacinia sit amet neque vel, finibus viverra nibh. Duis at turpis sed elit luctus dignissim a vel dolor. Praesent justo ex, semper sit amet augue ornare, rutrum rutrum mi. Sed laoreet est ante, id dapibus mi luctus et. Aenean venenatis nulla mi, a finibus odio laoreet vitae. Nulla ut dolor consectetur, feugiat erat nec, efficitur turpis. Curabitur consequat laoreet tortor, at varius eros tempor non. Integer neque sem, interdum vel ex quis, lacinia scelerisque mauris. In laoreet justo ante, quis tincidunt felis dapibus vel. Nullam vel ante urna. Duis velit neque, eleifend ut dapibus congue, pharetra at quam. Aenean enim eros, convallis eu lectus vitae, fermentum placerat risus. Proin suscipit ligula libero, et vestibulum massa tempus pharetra. Maecenas sed posuere nisi.</p>`
  return a
})

export default class ArticleAdapter extends ApplicationAdapter {
  findRecord (store, type, id, snapshot) {
    let article = ARTICLES.find(article => article.slug === id)
    if (!article) reject({ id })
    return article
  }

  query (store, type, query) {
    return ARTICLES
  }

  // async findRecord (store, type, id, snapshot) {
  //   try {
  //     let response = await fetch(`${this.baseURL}/data/models/article/content/${id}.md.json`)
  //     if (!response.ok) throw Error('Article not found')
  //     let model = await response.json()
  //     model.content = unescape(model.content)
  //     model.slug = id
  //     return model
  //   } catch (err) {
  //     console.error(err)
  //     return reject({ err, id })
  //   }
  // }

  // async query (store, type, query) {
  //   return [{
  //     slug: 'awesome-post',
  //     title: 'Awesome Post'
  //   }]
  // }
}
