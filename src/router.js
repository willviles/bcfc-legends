import EmberRouter from '@ember/routing/router'
import RouterScroll from 'ember-router-scroll'
import config from '../config/environment'

const Router = EmberRouter.extend(RouterScroll, {
  location: config.locationType,
  rootURL: config.rootURL
})

Router.map(function () {
  this.route('home', { path: '/' })
  this.route('page', { path: '/*slug' })

  this.route('articles', function () {
    this.route('item', { path: '/:slug' })
  })

  this.route('events', function () {
    this.route('item', { path: '/:slug' })
  })
})

export default Router
