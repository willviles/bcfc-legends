import Route from '@ember/routing/route'
import Router from '@ember/routing/router'
import { inject as service } from '@ember/service'
import { typeOf } from '@ember/utils'

export function initialize () {
  Router.reopen({
    headData: service(),
    setTitle (title) {
      this.headData.set('title', title)
    }
  })

  Route.reopen({
    headData: service(),
    router: service(),
    afterModel (model) {
      this._super(...arguments)
      let metadata = typeOf(this.metadata) === 'function'
        ? this.metadata(model) || {}
        : this.metadata
      if (metadata) this.headData.setProperties(metadata)
    }
  })
}

export default {
  name: 'head-data',
  initialize
}
