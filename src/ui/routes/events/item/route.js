import Route from '@ember/routing/route'

export default class EventsItemRoute extends Route {
  titleToken = 'Event'
  model = ({ slug }) => this.store.findRecord('event', slug)
}
