import Route from '@ember/routing/route'

export default class EventsRoute extends Route {
  titleToken = 'Events'

  setupController (controller, model) {
    super.setupController(...arguments)
    controller.setProperties({
      title: 'Events'
    })
  }
}
