import Controller from '@ember/controller'
import QueryParams from 'ember-parachute'
import { computed, defineProperty } from '@ember/object'
import { or } from '@ember/object/computed'
import { inject as service } from '@ember/service'

const QUERY_PARAMS_CONFIG = {
  timeframe: {
    as: 'timeframe',
    refresh: true,
    replace: true,
    defaultValue: 'upcoming'
  }
}

const QUERY_PARAMS = new QueryParams(QUERY_PARAMS_CONFIG)

export default Controller.extend(QUERY_PARAMS.Mixin, {
  store: service(),
  queryParams: ['timeframe'],

  init () {
    this._super(...arguments)
    defineProperty(
      this,
      'queryParamsChanged',
      or(`queryParamsState.{${Object.keys(QUERY_PARAMS_CONFIG).join(',')}}.changed`)
    )
  },

  timeframes: computed(function () {
    return ['upcoming', 'past']
  }),

  setup ({ queryParams }) {
    this.fetchEvents(queryParams)
  },

  queryParamsDidChange ({ shouldRefresh, queryParams }) {
    if (shouldRefresh) this.fetchEvents()
  },

  async fetchEvents () {
    const events = await this.store.query('event', {
      timeframe: this.timeframe
    })

    this.set('events', events)
  }
})
