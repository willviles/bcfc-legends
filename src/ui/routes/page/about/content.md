---
titleToken: About
title: Building a bridge between the club, the supporters and its history.
members:
  -
    name: Tom Ross
    bio: Etiam pretium ex id lectus consectetur, sed sodales urna egestas. Mauris malesuada congue libero id ultricies. Phasellus rhoncus urna eros, vel posuere dolor accumsan ut. Pellentesque urna eros, laoreet id faucibus tempor, egestas sit amet dui.
  -
    name: Will Viles
    bio: Quisque bibendum bibendum elementum. Donec dictum metus et ex maximus, in rhoncus magna rutrum. In rhoncus bibendum dui vel sollicitudin. Donec malesuada orci quis magna pretium gravida.
---

The formation of the Birmingham City Official Former Players Association is the culmination of a dream that Kevan Broadhurst and myself (Tom Ross) have had for many years. It all started with the formation of the Blues All Stars charity fundraising team of Ex-Players in 1991.

That first team included Kevan Broadhurst, Joe Gallagher, Micky Evans, Tom Ross, Terry Cooper, Ron Green, Trevor Morgan, Garry Pendrey, Robert Hopkins, Tony Evans, Tony Taylor and Steve Lynex to name but a few. Over the years, the Blues All Stars have raised around three quarters of a million pounds for many deserving charities around the Birmingham area.

During those years, we have had many supporters turn out for the team such as Steve Bruce, David Gold, Barry Fry and Noel Blake. The current team includes Geoff Horsfield, Martin O’Connor, Graham Hyde, Ian Clarkson, Paul Devlin, Phil Hawker, Dave Buust, Ron Green, Ian Atkins, Robert Hopkins, Guy Russell, and Micky Clarke.

About a year ago, Kevan and I discussed the logistics of forming a proper Former Players Association. The club bought into the idea and with the great help of committee member Jessica Birch and the club’s Head of Corporate Sales Adrian Wright we got it off the ground and the association now boasts over 100 members all ex-players from every era from the 1940′s through to the 2000′s.

It is our aim to find as many ex players as possible. The major reason for the association is to build a bridge between the club, the supporters and its history. It also allows the all ex-players to keep in contact with each other. By virtue of pitch and hospitality appearances, the Ex-players can keep in touch with the club’s fans.

We believe that every single ex-player who has worn the Royal blue shirt should be recognised and made welcome at St. Andrew’s.

We are also inviting every ex manager to join the Association with Barry Fry the first to accept membership. The formation of the association is a real positive step in the history of this great club.
