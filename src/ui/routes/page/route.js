import Route from '@ember/routing/route'
import fetch from 'fetch'

export default class PageRoute extends Route {
  titleToken = (model) => model.titleToken

  async model ({ slug }) {
    try {
      let response = await fetch(`/ui/routes/page/${slug}/content.md.json`)
      if (!response.ok) throw Error('Page not found')
      let model = await response.json()
      model.content = unescape(model.content)
      return Object.assign({}, {
        template: `page/${slug}`
      }, model)
    } catch (err) {
      return {
        title: '404',
        template: '404',
        slug
      }
    }
  }

  renderTemplate (controller, model) {
    this.render(model.template, {
      into: 'application'
    })
  }
}
