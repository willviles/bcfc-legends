import Component from '@ember/component'
import { service } from '@ember-decorators/service'

export default class AllStarsFixturesComponent extends Component {
  @service('store') store

  didInsertElement () {
    super.didInsertElement(...arguments)
    this.fetchEvents()
  }

  async fetchEvents () {
    const events = await this.store.query('event', {
      // timeframe: 'upcoming',
      tags: ['all-stars-fixture'],
      limit: 3
    })

    this.set('events', events)
  }
}
