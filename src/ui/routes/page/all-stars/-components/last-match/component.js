import Component from '@ember/component'
import { tagName } from '@ember-decorators/component'
import { service } from '@ember-decorators/service'

@tagName('')
export default class LastMatchComponent extends Component {
  @service('store') store

  didInsertElement () {
    super.didInsertElement(...arguments)
    this.fetchData()
  }

  async fetchData () {
    const events = await this.store.query('event', {
      timeframe: 'past',
      tags: ['all-stars-fixture'],
      limit: 1
    })

    console.log(events)

    this.set('event', events.firstObject)
  }
}
