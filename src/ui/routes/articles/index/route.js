import Route from '@ember/routing/route'
import { hash } from 'rsvp'

export default class ArticlesIndexRoute extends Route {
  titleToken = 'Articles'

  model () {
    return hash({
      articles: this.store.query('article', {})
    })
  }

  setupController (controller, model) {
    super.setupController(...arguments)
    controller.setProperties({
      title: 'Articles'
    })
  }
}
