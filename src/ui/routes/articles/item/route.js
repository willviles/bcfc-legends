import Route from '@ember/routing/route'

export default class ArticlesItemRoute extends Route {
  titleToken = (model) => model.title
  model = ({ slug }) => this.store.findRecord('article', slug)
}
