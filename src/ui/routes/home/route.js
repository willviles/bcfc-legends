import Route from '@ember/routing/route'
import { hash } from 'rsvp'

export default class ApplicationRoute extends Route {
  titleToken = 'Home'

  model () {
    return hash({
      articles: this.store.query('article', {
        limit: 3
      })
    })
  }

  setupController (controller) {
    super.setupController(...arguments)
    controller.setProperties({
      title: `Birmingham City Former Players' Association`
    })
  }
}
