import Component from '@ember/component'
import { tagName } from '@ember-decorators/component'
import { service } from '@ember-decorators/service'

@tagName('')
export default class UpcomingEventsComponent extends Component {
  @service('store') store

  didInsertElement () {
    super.didInsertElement(...arguments)
    this.fetchEvents()
  }

  async fetchEvents () {
    const events = await this.store.query('event', {
      timeframe: 'upcoming',
      limit: 3
    })

    this.set('events', events)
  }
}
