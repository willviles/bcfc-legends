import Component from '@ember/component'
import { tagName } from '@ember-decorators/component'
import { service } from '@ember-decorators/service'

@tagName('')
export default class LatestArticlesComponent extends Component {
  @service('store') store

  didInsertElement () {
    super.didInsertElement(...arguments)
    this.fetchData()
  }

  dataKey = 'articles'

  async fetchData () {
    const data = await this.store.query('article', {
      limit: 3
    })

    this.set(this.dataKey, data)
  }
}
