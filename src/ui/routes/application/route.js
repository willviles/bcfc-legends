import Route from '@ember/routing/route'

export default class ApplicationRoute extends Route {
  appName = 'BCFC Legends'
  description = 'Vestibulum finibus gravida leo vitae mollis. Quisque consequat imperdiet lacus, vitae tincidunt dui.'
  keywords = 'birmingham,city,former,players,association,keep,right,on,history'
  copyright = `© Birmingham City Former Players' Association`

  title (tokens) {
    return tokens.length
      ? `${tokens.reverse().join(' – ')} – ${this.appName}`
      : this.appName
  }

  metadata () {
    return this.getProperties([
      'appName',
      'description',
      'keywords',
      'copyright'
    ])
  }
}
