import Component from '@ember/component'
import { tagName } from '@ember-decorators/component'
import { service } from '@ember-decorators/service'
import layout from '../../routes/application/head'

@tagName('')
export default class HeadContentComponent extends Component {
  @service('headData') model
  layout = layout
}
