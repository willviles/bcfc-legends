import Component from '@ember/component'
import { argument } from '@ember-decorators/argument'
import { tagName } from '@ember-decorators/component'
import { action, computed } from '@ember-decorators/object'
import { service } from '@ember-decorators/service'

const HIDDEN_ROUTES = ['home']

@tagName('')
export default class NavbarComponent extends Component {
  @service('router') router
  @service('sideMenu') sideMenu

  @argument name = null
  @argument canHide = true
  @argument logoHeight = '40px'

  @computed('router._router.currentRouteName')
  get isHidden () {
    const currentRouteName = this.get('router._router.currentRouteName')
    if (!currentRouteName || !this.canHide) return
    return HIDDEN_ROUTES.includes(currentRouteName)
  }

  @action
  toggleSideMenu () {
    this.sideMenu.toggle()
  }
}
