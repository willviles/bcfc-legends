import Component from '@ember/component'
import { tagName } from '@ember-decorators/component'
import { computed } from '@ember-decorators/object'

@tagName('')
export default class FooterComponent extends Component {
  @computed
  get year () {
    return new Date().getFullYear()
  }
}
