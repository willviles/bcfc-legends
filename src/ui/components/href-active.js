import Helper from '@ember/component/helper'
import { getOwner } from '@ember/application'
import { service } from '@ember-decorators/service'

function hrefActive (context, params) {
  const owner = getOwner(context)
  const routeParams = params.reduce((p, param, i) => {
    if (i === 0) {
      if (owner.mountPoint && param !== 'application') {
        param = `${owner.mountPoint}.${param}`
      }
    }
    return [...p, param]
  }, [])
  return context.router.isActive(...routeParams)
}

export const helper = class HrefActiveHelper extends Helper {
  @service('router') router

  init () {
    super.init()
    this.addObserver(`router.currentURL`, this, 'recompute')
  }

  compute (params, namedArgs) {
    if (namedArgs.params) {
      return hrefActive(this, namedArgs.params)
    } else {
      return hrefActive(this, params)
    }
  }

  willDestroy () {
    this.removeObserver(`router.currentURL`, this, 'recompute')
  }
}
