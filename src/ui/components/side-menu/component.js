import SideMenu from 'ember-side-menu/components/side-menu'
import { argument } from '@ember-decorators/argument'

export default class SideMenuComponent extends SideMenu {
  @argument side = 'right'

  didInsertElement () {
    super.didInsertElement(...arguments)
    this.element.addEventListener('click', this.toggleSideMenuListener.bind(this))
  }

  willDestroyElement () {
    super.willDestroyElement(...arguments)
    this.element.removeEventListener('click', this.toggleSideMenuListener.bind(this))
  }

  toggleSideMenuListener (e) {
    if (e.target.classList.contains('toggle-side-menu')) this.sideMenu.toggle()
  }

  toggleSideMenu () {
    this.sideMenu.toggle()
  }
}
