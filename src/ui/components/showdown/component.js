import MarkdownToHTMLComponent from 'ember-cli-showdown/components/markdown-to-html'
import { tagName } from '@ember-decorators/component'

@tagName('')
export default class ShowdownComponent extends MarkdownToHTMLComponent {

}
