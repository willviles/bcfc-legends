import Helper from '@ember/component/helper'
import { getOwner } from '@ember/application'
import { warn } from '@ember/debug'

function hrefTo (context, targetRouteName, ...rest) {
  let owner = getOwner(context)

  // If used in an engine, namespace the route
  if (owner.mountPoint && targetRouteName !== 'application') {
    targetRouteName = `${owner.mountPoint}.${targetRouteName}`
  }
  return getHrefFromOwner(owner, targetRouteName, ...rest)
}

function getHrefFromOwner (owner, targetRouteName, ...rest) {
  try {
    let router = owner.lookup('service:router')

    if (router === undefined) return

    let lastParam = rest[rest.length - 1]

    let queryParams = {}
    if (lastParam && lastParam.isQueryParams) {
      queryParams = rest.pop()
    }

    let args = [targetRouteName]
    args.push.apply(args, rest)
    args.push({ queryParams: queryParams.values })

    return router.urlFor.apply(router, args)
  } catch (err) {
    warn(err.message, { id: 'unsolved-href-to-error' })
  }
}

export { hrefTo, getHrefFromOwner }

export const helper = class HrefToHelper extends Helper {
  compute ([targetRouteName, ...rest], namedArgs) {
    if (namedArgs.params) {
      return hrefTo(this, ...namedArgs.params)
    } else {
      return hrefTo(this, targetRouteName, ...rest)
    }
  }
}
