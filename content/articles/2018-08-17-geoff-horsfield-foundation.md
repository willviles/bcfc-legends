---
title: Geoff Horsfield Foundation continues good work
subtitle: Sean Cole catches up with Barnsley-born striker Geoff Horsfield to look back on his career, which included a memorable three years at St. Andrew's.
author: Sean Cole
---

He also finds out more about the former Blues favourite’s current endeavours, including setting up a Foundation that helps the homeless of Birmingham…

The success of Charlie Austin and Jamie Vardy amongst others has brought the issue of footballers who rise from non-league to the top flight back into focus. It proves that there are players out there with the right talent and mentality who just need a chance. For some, that never comes. For others, it simply takes a little longer than they’d hoped.

Back in 2002, another striker completed the same journey, having scored in every division along the way. Geoff Horsfield was the archetypal English centre-forward – battering ram and bullish number nine. He was 29 when he finally got the chance to play in the Premier League and determined to make up for lost time.

Born and raised in Barnsley, Horsfield was instilled with a strong work ethic. In a football sense, he had to grow up fast, playing in a men’s league from the age of 13 to toughen him up after being rejected by his hometown team. It certainly worked, as he was scouted by Scarborough at 16. Horsfield scored two goals in a game against the club’s first team and was offered professional forms. His dad advised against it, saying he should get a trade first. He combined a bricklaying apprenticeship with playing part-time.

“I used to work from seven or eight in the morning until four or five at night,” says Horsfield. “Doing my apprenticeship, going to college, coming back. But every Friday, when I was playing for Scarborough on the weekend, I’d finish at three o’clock and get the train up. I used to get into Scarborough about half ten at night. I’d have kebab and chips and walk for a mile from the train station to my digs because they wouldn’t pay for a taxi.”

It was a busy schedule and a simpler time. Still Horsfield prospered until an unfortunate turn of events. He scored eight minutes into his first team debut but after 12 games the Scarborough manager, Ray McHale, got sacked and Steve Wicks took over. Horsfield was released and forced to go through the non-league route, with his bricklaying job to fall back on.

He worked his way back up and finally broke into the Football League in 1998 as his goals helped Halifax Town to the Conference title. Having taken a pay cut when he gave up his day job in order to go professional, fortunately it wasn’t long before Horsfield was being offered the chance to make a life-changing move. After seven goals in 10 Third Division games, Fulham bid £300,000 for him.

LONDON CALLING
“I met Kevin Keegan and I was in awe of him. He offered me a contract but with bonuses I was on more at Halifax than I would have been at Fulham. I sat there, and he said, ‘Do you want to sign?’ I went, ‘Nah, I’m sorry. I can’t move down to London where it’s more expensive for that.’ Halifax were gutted because they were struggling for money at the time,” says Horsfield.

He travelled back up north but received a call later that night. Fulham were willing to increase their offer and Keegan promised to rip up Horsfield’s contract and double what he was on if things went well. True to his word, after a couple of months, and plenty of goals, he was given a new deal. It was an exciting time for the club and Horsfield personally as he embraced the London nightlife.

Fulham had a strong group of players, including Maik Taylor, Steve Finnan, Chris Coleman and Barry Hayles. They cruised to the title with Horsfield as the club’s leading goalscorer and started life in the Division One against Birmingham City at St. Andrew’s. Horsfield was heavily involved, scoring twice before being sent off late in the second-half.

SEEING RED BEFORE TURNING BLUE
“I was playing against Michael Johnson and Pursey (Darren Purse). There was me and Stan Collymore up front,” he recalls. “I got booked for getting Gary Rowett around the throat. Then Jonno’s gone to put a ball down the line and I’ve gone straight through him. I pushed him into the boards and I thought, ‘I’m getting sent off here.’ Boom – red card. I can always remember the hostility as I walked 40 yards off the pitch. I was getting dog’s abuse.”

It was a volatile introduction to the Blues supporters, but Horsfield admired their passion and commitment. Within a year they were cheering for him, as he became the Club’s record signing. A £2.25 million deal was agreed, and sights were set on promotion. Despite Trevor Francis’ best efforts, it wasn’t until a change of manager that they finally made it to the Premier League.

“It got a bit stale under Trevor, but Brucie came in and gave us a good 10 days of brutal running and fitness work. We ended up winning game after game. Getting through to the Play-Off Final was fantastic and then the trip to Cardiff was absolutely brilliant, with me scoring as well. Unbelievable. We played on the Sunday and I don’t think I went home until the following Saturday or Sunday! I was out up Broad Street, Brindley Place, all over. It was a fantastic time for us all.”

With the arrivals of Stern John and Clinton Morrison, Geoff Horsfield didn’t get to start too regularly in the top flight but was often used to great effect off the bench. In a fierce derby against Aston Villa he grabbed his first ever goal at that level to secure a remarkable 3-0 win. It was an emotional occasion that the former striker remembers with great fondness. The Blues players were well up for it.

DOUBLE DERBY DELIGHT
“Olof Mellberg had done an interview where he said he'd never heard of Birmingham City or any of their players and if they got their minds right they'd beat Birmingham City convincingly. Brucie just said 'We've done all the pre-match and all the stuff on set-plays, now read that' and he just pinned it up. And all our lads were like 'I'm not having this,' so that was all the motivation we needed,” says Horsfield.

“On paper Villa should have easily beaten us because they'd got recognised Premier League players and full internationals. But I've always said it, we beat them in that tunnel. All of us were raging, growling and shouting. I think there was only big Dion (Dublin), who wasn't in the squad because he was suspended or injured. He was saying, ‘Come on lads, face these’ and all that but there weren't many of them saying anything. They were quiet as anything.”

In the return match at Villa Park, Horsfield struck again after another Peter Enckelman mistake and celebrated in front of the Holte End. He then went in goal when Nico Vaesen snapped his cruciate ligament. With the home side down to 10 men, he preserved a clean sheet as Blues held out to do the double over their rivals. Horsfield often saved his best for derby games and continued to torment Villa when he went on to join West Brom.

At the Hawthorns he was part of the great escape under Bryan Robson, as they became the first club to avoid relegation from the Premier League having been bottom of the table at Christmas. Horsfield scored with his first touch after coming on against Portsmouth on the final day of the season, and then set up Kieran Richardson for the second goal. He was mobbed by supporters who invaded the pitch as results elsewhere confirmed their survival.

In later years there were spells at Sheffield United, Leeds United and Lincoln City amongst others. Horsfield retired from playing at Port Vale, where he stayed on as assistant manager. Since falling out of love with coaching he’s set up the Geoff Horsfield Foundation, which operates across the West Midlands to help the homeless get back on their feet by providing them with safe and secure supported accommodation, as well as weekly food donations.

FOUNDATION FOR THE FUTURE
“I set up a maintenance company four years ago because I've always been pretty handy that way,” says Horsfield. “A lady got in contact with me and asked me to do a bit of decorating and some flooring for some vulnerable adults, and as a result of that I thought something needs to be done about these sorts of situations, so I set up the Foundation and we started doing jobs for vulnerable adults.

“Some had got mental health issues, drug or alcohol addiction, and others were homeless, things like that. Our aim is to provide them with accommodation and help them get access to the medication, benefits, training and other support that can help turn their lives around. Ultimately, we want to help them live independently in their own flats and get jobs, so they can support themselves and break away from the tough situations they have found themselves in.”

As part of this work, Horsfield and around 40 others recently slept rough outside St. Andrew’s to increase awareness of the issue and raise funds for his Foundation. He’s keen for it to expand and start making a difference on an even bigger scale. A Birmingham City cult hero admired for his commitment and common touch, he still has those attributes in abundance.
