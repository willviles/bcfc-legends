'use strict'

const EmberApp = require('ember-cli/lib/broccoli/ember-app')
const resolveMarkdown = require('./utils/markdown-resolver')

module.exports = function (defaults) {
  let config = {}

  /**
   * @ref https://github.com/ef4/prember#options
   */
  config['prember'] = {
    async urls ({ distDir, visit }) {
      let staticUrls = [
        `/`
      ]

      return [...staticUrls]
    }
  }

  /**
   * @ref https://github.com/ivanvotti/ember-svg-jar#usage
   */

  config['svgJar'] = {
    strategy: ['inline'],
    inline: {
      sourceDirs: [
        'public/icons/svg',
        'public/images/svg'
      ]
    }
  }

  let app = new EmberApp(defaults, config)

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  resolveMarkdown(app, {
    collections: {
      articles: {
        inputPath: `content/articles`,
        publicPath: `articles`,
        generateImports: true,
        recursive: true
      }
    },
    src: {
      enabled: true,
      generateImports: true
    }
  })

  return app.toTree()
}
